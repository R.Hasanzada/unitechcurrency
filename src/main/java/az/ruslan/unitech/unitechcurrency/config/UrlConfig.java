package az.ruslan.unitech.unitechcurrency.config;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Getter
@Configuration
public class UrlConfig {

    @Value("${endpoint.exchange_rate}")
    private String exchangeUrl;

    @Value("${endpoint.currency_rates}")
    private String currencyRatesUrl;
}
