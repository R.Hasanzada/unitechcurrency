package az.ruslan.unitech.unitechcurrency;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class UniTechCurrencyApplication {

	public static void main(String[] args) {
		SpringApplication.run(UniTechCurrencyApplication.class, args);
	}

}
