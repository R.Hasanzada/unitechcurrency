package az.ruslan.unitech.unitechcurrency.controller;

import az.ruslan.unitech.unitechcurrency.service.RateService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("exchange")
public class ExchangeController {

    private final RateService exchangeService;

    @GetMapping("/{from}/{to}")
    public ResponseEntity<?> getExchange(@PathVariable String from,
                                         @PathVariable String to){
        return ResponseEntity.ok().body(exchangeService.getRate(from, to));
    }

    @GetMapping("/all")
    public ResponseEntity<?> getAllRates(){
        return ResponseEntity.ok().body(exchangeService.getCurrencyAllRates());
    }
}
