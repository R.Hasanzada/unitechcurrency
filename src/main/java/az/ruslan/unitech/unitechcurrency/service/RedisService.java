package az.ruslan.unitech.unitechcurrency.service;

import az.ruslan.unitech.unitechcurrency.dto.CurrencyAllRates;
import az.ruslan.unitech.unitechcurrency.enums.RedisKey;
import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.TimeUnit;

@Service
@RequiredArgsConstructor
public class RedisService {

    private final RedisTemplate redisTemplate;

    public <T> void save(RedisKey key, T data){
        Long timeValue = 60L;
        TimeUnit timeUnit = TimeUnit.SECONDS;
        redisTemplate.opsForValue().set(key, data, timeValue, timeUnit);
    }

    public <T> T get(RedisKey key){
        return (T) redisTemplate.opsForValue().get(key);
    }

    public List<CurrencyAllRates> getAllRate(RedisKey key){
        return (List<CurrencyAllRates>) redisTemplate.opsForValue().get(key);
    }
}
