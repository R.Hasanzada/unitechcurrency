package az.ruslan.unitech.unitechcurrency.service;


import az.ruslan.unitech.unitechcurrency.client.RateClient;
import az.ruslan.unitech.unitechcurrency.dto.CurrencyAllRates;
import az.ruslan.unitech.unitechcurrency.dto.CurrencyRate;
import az.ruslan.unitech.unitechcurrency.enums.Currency;
import az.ruslan.unitech.unitechcurrency.enums.RedisKey;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class RateService {

    private final RateClient rateClient;
    private final RedisService redisService;

    public CurrencyRate getRate(String from, String to){
        List<CurrencyAllRates> allRate = redisService.getAllRate(RedisKey.RATES);
        if(allRate != null && !allRate.isEmpty()){
            Map.Entry<String, BigDecimal> stringBigDecimalEntry = allRate.stream()
                    .filter(currencyAllRates -> currencyAllRates.getCurrencyFrom().equals(from))
                    .flatMap(currencyAllRates -> currencyAllRates.getRate().entrySet().stream())
                    .filter(entry -> entry.getKey().equals(to))
                    .findFirst().get();
            CurrencyRate currencyRate = new CurrencyRate();
            currencyRate.setCurrencyFrom(from);
            currencyRate.setCurrencyTo(to);
            currencyRate.setRate(stringBigDecimalEntry.getValue());
            return currencyRate;
        }else
             return rateClient.getRate(from.toUpperCase(), to.toUpperCase());
    }


    @Scheduled(fixedRate = 60000)
    //@Scheduled(fixedRate = 120000)
    public void updateRatesCash(){
        System.err.println("Updating rates cache");
        redisService.save(RedisKey.RATES, getCurrencyAllRates());
    }



    public List<CurrencyAllRates> getCurrencyAllRates(){
        List<Currency> currencyList = List.of(
                Currency.AZN,
                Currency.USD,
                Currency.EUR,
                Currency.TRY
        );
        List<CurrencyAllRates> currencyAllRatesList = currencyList.stream()
                .map(currency -> rateClient.getRatesForGivenCurrencies(currency.name().toUpperCase()))
                .collect(Collectors.toList());
        return currencyAllRatesList;
    }
}
