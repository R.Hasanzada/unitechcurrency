package az.ruslan.unitech.unitechcurrency.enums;

public enum Currency {
    AZN, USD, EUR, TRY;
}
