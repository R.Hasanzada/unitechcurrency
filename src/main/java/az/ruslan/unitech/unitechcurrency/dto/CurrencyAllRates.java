package az.ruslan.unitech.unitechcurrency.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Map;

@Data
public class CurrencyAllRates implements Serializable {

    @JsonProperty("base_code")
    private String currencyFrom;

    @JsonProperty("conversion_rates")
    private Map<String, BigDecimal> rate;
}
