package az.ruslan.unitech.unitechcurrency.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.math.BigDecimal;

@Data
public class CurrencyRate {

    @JsonProperty("base_code")
    private String currencyFrom;
    @JsonProperty("target_code")
    private String currencyTo;
    @JsonProperty("conversion_rate")
    private BigDecimal rate;
}
