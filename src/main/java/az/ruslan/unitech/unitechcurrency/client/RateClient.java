package az.ruslan.unitech.unitechcurrency.client;

import az.ruslan.unitech.unitechcurrency.config.UrlConfig;
import az.ruslan.unitech.unitechcurrency.dto.CurrencyAllRates;
import az.ruslan.unitech.unitechcurrency.dto.CurrencyRate;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class RateClient {

    private final RestTemplate restTemplate;
    private final UrlConfig urlConfig;
    private final String MY_API_KEY = "2834fe91a9ad5db831232fd7";

    public CurrencyRate getRate(String from, String to){
        String exchangeUrl = urlConfig.getExchangeUrl();
        String url = String.format(exchangeUrl, MY_API_KEY, from, to);
        return restTemplate.getForEntity(url, CurrencyRate.class).getBody();
    }

    public CurrencyAllRates getRatesForGivenCurrencies(String currency){
        String currencyRatesUrl = urlConfig.getCurrencyRatesUrl();
        String url = String.format(currencyRatesUrl, MY_API_KEY, currency);
        CurrencyAllRates currencyAllRates = restTemplate.getForEntity(url, CurrencyAllRates.class).getBody();
        return currencyAllRates;
    }

}
